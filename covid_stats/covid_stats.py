""" Simple notification alert with the actual COVID-19 stats for Euskirchen. """
import datetime
import time
import json
import requests
import locale
from plyer import notification

locale.setlocale(locale.LC_ALL, "German")

content = """
Inzidenz (LK):\t{district_incidence}
Inzidenz (BL):\t{state_incidence}
Hosp.-Rate:\t{hosp_quote}
"""


def get_data(url):
    try:
        resp = requests.get(url)
        if resp.status_code == 200:
            data = resp.json()
        else:
            data = None
        return data
    except requests.RequestException as e:
        print("An error occured: %s." % e)
        return None


def get_covid_data_for_district(district):
    district_data = get_data("https://api.corona-zahlen.org/districts/%s/" % district)
    if district_data is not None:
        district_name = district_data["data"][district]["name"]
        state = district_data["data"][district]["stateAbbreviation"]
        district_incidence = district_data["data"][district]["weekIncidence"]

        state_data = get_data("https://api.corona-zahlen.org/states/%s/" % state)
        state_name = state_data["data"][state]["name"]
        state_incidence = state_data["data"][state]["weekIncidence"]
        hosp_quote = state_data["data"][state]["hospitalization"]["incidence7Days"]

        data = {
            "district": district_name,
            "state": state_name,
            "district_incidence": locale.format_string(
                "%.2f", float(district_incidence), True
            ),
            "state_incidence": locale.format_string(
                "%.2f", float(state_incidence), True
            ),
            "hosp_quote": locale.format_string("%.2f", float(hosp_quote), True),
        }
    else:
        data = None
    return data


def send_notification(data):
    notification.notify(
        title="Aktuelle COVID-Zahlen ({district})".format(district=data["district"]),
        message=content.format(
            district_incidence=data["district_incidence"],
            state_incidence=data["state_incidence"],
            hosp_quote=data["hosp_quote"],
        ),
        app_name="COVID Daten",
        app_icon="./alert.ico",
        timeout=10,
    )


if __name__ == "__main__":
    district = "05366"
    data = get_covid_data_for_district(district)
    if data is not None:
        while True:
            send_notification(data)
            time.sleep(60 * 60 * 4)
    else:
        notification.notify(title="An error occured. Retrying..", app_icon="alert.ico")
        time.sleep(60)
