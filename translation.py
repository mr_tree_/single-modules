""" Module for translation with IBM Watson """
from ibm_watson import LanguageTranslatorV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

# API
API_KEY = "0lXYR3vrNqHNpA6I6JgVZ7oJ7b7exl9LagOqY3u18cBt"
API_URL = "https://api.eu-de.language-translator.watson.cloud.ibm.com/instances/296ebbfc-64e9-4188-a3a9-dcd9aa85a226"


def authenticate():
    """ Authenticate on IBM Watson API """
    authenticator = IAMAuthenticator(API_KEY)
    ts = LanguageTranslatorV3(version="2018-05-01", authenticator=authenticator)
    ts.set_service_url(API_URL)
    return ts


def translate(ts, origin, target, source="de"):
    """ Translate origin text to target language with IBM Watson """
    translation = ts.translate(text=origin, source=source, target=target).get_result()
    return translation["translations"][0]["translation"]
