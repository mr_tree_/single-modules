""" Small script for getting system information over WMI. """
import wmi

c = wmi.WMI()
system = c.Win32_ComputerSystem()[0]

print("Manufacturer: {}".format(system.Manufacturer))
print("Model: {}".format(system.Model))
print("Name: {}".format(system.Name))
print("NumberOfProcessors: {}".format(system.NumberOfProcessors))
print("SystemType: {}".format(system.SystemType))
print("SystemFamily: {}".format(system.SystemFamily))