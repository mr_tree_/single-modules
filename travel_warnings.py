""" Get all countries with actual travel warnings or partial travel warnings connecteed to COVID-19 """
import requests
from tabulate import tabulate

if __name__ == "__main__":
    # OpenData Doc:  https://www.auswaertiges-amt.de/open-data
    url = "https://auswaertiges-amt.de/opendata/travelwarning/"

    # Get JSON data from API
    travel_data = None
    try:
        travel_data = requests.get(url)
    except requests.RequestException as e:
        print("An error occured: %s" % e)

    # Check data for all IDs which got a travel or partial travel warning
    covid_warnings = []
    if travel_data is not None:
        data = travel_data.json()["response"]
        for content_id in data["contentList"]:
            if (
                data[content_id]["situationWarning"]
                or data[content_id]["situationPartWarning"]
            ):
                country = data[content_id]["title"].split(":")[0]
                url = "https://auswaertiges-amt.de/de/%s" % content_id
                covid_warnings.append([country, url])

    # Sort COVID warnings alphabetically and output a tabled list with names and URLs
    sorted_list = sorted(covid_warnings, key=lambda x: (x[0], x[1]))
    print(tabulate(sorted_list, headers=["country", "url"]))
