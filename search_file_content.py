""" This module searches for a string in files. """
import os

BINARY_MODE = "rb"
TEXT_MODE = "r"


def search_in_file(search_str, path_to_file, found_list, error_list, open_mode):
    """ This method searches for a specific string in the file content, depending on the mode. """
    try:
        file_content = open(path_to_file, open_mode)
        content = str(file_content.read())
    except PermissionError:
        content = ""
        if path_to_file not in error_list["permission_errors"]:
            error_list["permission_errors"].append(path_to_file)
    except UnicodeDecodeError:
        content = ""
        if path_to_file not in error_list["unicode_errors"]:
            error_list["unicode_errors"].append(path_to_file)

    if search_str.lower() in content.lower() and path_to_file not in found_list:
        found_list.append(path_to_file)


def error_statistics(error_list):
    """ This method prints out some statistics for the errors while searching.  """
    sum_unicode_errors = len(error_list["unicode_errors"])
    sum_permission_errors = len(error_list["permission_errors"])
    sum_errors = sum_unicode_errors + sum_permission_errors

    while True:
        option = input(
            "\nDo you like to see the {} errors in detail? (y / n [default]): ".format(
                sum_errors
            )
        )
        if option == "y":
            print("{} UnicodeDecodeErrors in total:".format(sum_unicode_errors))
            for unicode_error in errors["unicode_errors"]:
                print("  > UnicodeDecodeError: {}".format(unicode_error))
            print("\n{} PermissinoErrors in total:".format(sum_permission_errors))
            for permission_error in errors["permission_errors"]:
                print("  > PermissionError: {}".format(permission_error))
        elif option in ["n", ""]:
            break
        else:
            print("Not valid. Please try again.")


if __name__ == "__main__":
    found_files = []
    errors = {
        "unicode_errors": [],
        "permission_errors": [],
    }
    modes = [
        BINARY_MODE,
        TEXT_MODE,
    ]

    while True:
        SEARCH_DIR = input("Which directory you would like to check?: ")
        if os.path.exists(SEARCH_DIR):
            break
        else:
            print("Directory not found. Please try again.")

    search_for = input("What are you searching for?: ")

    files = os.listdir(SEARCH_DIR)
    for file_name in files:
        file_path = os.path.join(SEARCH_DIR, file_name)
        for mode in modes:
            search_in_file(search_for, file_path, found_files, errors, mode)

    if len(found_files) > 0:
        print("Done. {} files found!".format(len(found_files)))
        for found_file in found_files:
            print("  > {}".format(found_file))
    else:
        print("Done. No matches found.")

    error_statistics(errors)
