""" Module for converting PDF files to JPGs """
import os
from pdf2image import convert_from_path

if __name__ == "__main__":
    while True:
        DIR = input("From which directory you want to convert PDFs?: ")
        if os.path.exists(DIR):
            files = os.listdir()
            for file in files:
                name, ext = file.split(".")
                if ext == "pdf":
                    images = convert_from_path(file)
                    for image in images:
                        image.save(name + ".jpg", "JPEG")
            break
        else:
            print("Something went wrong. Directory not found.")
