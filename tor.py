import requests
from stem import Signal
from stem.control import Controller

PROXIES = {"http": "socks5://127.0.0.1:9050", "https": "socks5://127.0.0.1:9050"}


def get_ip_address(proxies=PROXIES):
    return requests.get("https://api.ipify.org", proxies=proxies).text


def obtain_new_identity():
    c = Controller.from_port()
    c.authenticate()
    c.signal(Signal.NEWNYM)


if __name__ == "__main__":
    old_ip_address = get_ip_address()
    obtain_new_identity()
    new_ip_address = get_ip_address()
    print("Obtain new identity: {} -> {}".format(old_ip_address, new_ip_address))
